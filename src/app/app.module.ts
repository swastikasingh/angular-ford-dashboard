import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BootstrapModalModule } from 'ng2-bootstrap-modal';

import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';

import { DashboardComponent } from '../dashboard/dashboard/dashboard.component';
import { CarDetailsComponent } from '../car-details/car-details/car-details.component';
import { DashboardModule } from '../dashboard/dashboard.module';
import { CarDetailsModule } from '../car-details/car-details.module';

const appRoutes: Routes = [
  { path: '', component: DashboardComponent },
  { path: 'carDetails', component: CarDetailsComponent }
 ];
 

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(appRoutes),
    DashboardModule,
    CarDetailsModule,
    BootstrapModalModule,
    
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
