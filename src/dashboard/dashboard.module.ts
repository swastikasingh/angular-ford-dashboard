import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';

import { FilterComponent } from './filter/filter.component';
import { CarCardListComponent } from './car-card-list/car-card-list.component';
import { ColorComponent } from './color/color.component';
import { PriceComponent } from './price/price.component';
import { CardListComponent } from './card-list/card-list.component';
import { DashboardComponent } from './dashboard/dashboard.component';

@NgModule({
  imports: [
    CommonModule,
    HttpClientModule
  ],
  
  declarations: [FilterComponent, CarCardListComponent, ColorComponent, PriceComponent, CardListComponent, DashboardComponent],
  exports: [CardListComponent, CarCardListComponent, FilterComponent, DashboardComponent, RouterModule]
})
export class DashboardModule { }
