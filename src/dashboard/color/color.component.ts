import { Component, OnInit } from '@angular/core';
import { DashboardService} from '../dashboard.service';
import { Config } from 'protractor';

@Component({
  selector: 'app-color',
  templateUrl: './color.component.html',
  styleUrls: ['./color.component.css']
})
export class ColorComponent implements OnInit {
  colorCode = "";
  baseCarUrl;
  filteredCarDetails;
  response;
  colorId;
  constructor(private dashboardService: DashboardService) { }
  exteriorColor: Config;
  getExteriorColor(){
    this.dashboardService.getExteriorColor()
    .subscribe((data: Config) => {
        this.exteriorColor = data.Response.FilterDefinitions.FilterDefinition[2].FilterItems.FilterItem
      })
    
  }
  

  colorFilter(id){
    // this.baseCarUrl = "http://wwwqa.servicesus.ford.com/inventory/Search.json?" +
    //            "make=Ford&model=F-150;Fiesta;Mustang;Fusion;Transit&year=" +
    //            "2017&dealerPACode=05453;09158&postalCode=90210&exteriorColor="+id;
    // this.dashboardService.getFilteredCars(this.baseCarUrl)
    //   .subscribe((data: Config) => {
    //     console.log(data.Response)
    //     this.filteredCarDetails = data.Response.VehicleSearch.Vehicles.Vehicle
    //   })
      this.dashboardService.dataSource.next(id);

  }

  ngOnInit() {
    this.getExteriorColor();
  }

}
