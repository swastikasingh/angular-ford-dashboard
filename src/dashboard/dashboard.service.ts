import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DashboardService {

  public dataSource = new BehaviorSubject<String>('');
  
  
  constructor(private http: HttpClient) { }
  baseCarUrl = "http://wwwqa.servicesus.ford.com/inventory/Search.json?"+
                "make=Ford&model=F-150;Fiesta;Mustang;Fusion;"+
                "Transit&year=2017&dealerPACode=05453;09158&postalCode=90210";
  exteriorColorUrl = "http://wwwqa.servicesus.ford.com/inventory/FilterItems.json?"+
                      "make=Ford&model=Mustang&year=2017";
  getCarDetails() {
    return this.http.get(this.baseCarUrl);
  }
  getExteriorColor(){
    return this.http.get(this.exteriorColorUrl);
  }
  getFilteredCars(url){
    return this.http.get(url);
  }
}

