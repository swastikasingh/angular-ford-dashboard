import { Component, OnInit } from '@angular/core';
import { DashboardService} from '../dashboard.service';
import { Config } from 'protractor';
import { Router } from '@angular/router';

@Component({
  selector: 'app-card-list',
  templateUrl: './card-list.component.html',
  styleUrls: ['./card-list.component.css']
})
export class CardListComponent implements OnInit {
  carDetails: Config;
  
  constructor(private dashboardService: DashboardService,private router: Router) { }

  prefixUrl = "http://build.ford.com/dig/Ford/";
  midUrl = "/HD-FULL/";
  suffixUrl = "/EXT/1/vehicle.png?r=NaN";
  imgUrl = "/";
  baseCarUrl;
  filteredCarDetails;
  getCarDetails() {
    this.dashboardService.getCarDetails()
      .subscribe((data: Config) => {
        this.carDetails = data.Response.VehicleSearch.Vehicles.Vehicle
    })
  }

  

  carDetailsPage(vin) {
    this.router.navigate(['carDetails'],{ queryParams: { vin: vin } })
  }
  
  ngOnInit() {
    this.getCarDetails();
    this.dashboardService.dataSource.subscribe((data:String) =>{
      this.baseCarUrl = "http://wwwqa.servicesus.ford.com/inventory/Search.json?" +
               "make=Ford&model=F-150;Fiesta;Mustang;Fusion;Transit&year=" +
               "2017&dealerPACode=05453;09158&postalCode=90210&exteriorColor="+data;
      this.dashboardService.getFilteredCars(this.baseCarUrl)
        .subscribe((carData: Config) => {
          console.log(carData.Response)
          this.carDetails = carData.Response.VehicleSearch.Vehicles.Vehicle
        })
    })
  }
}
