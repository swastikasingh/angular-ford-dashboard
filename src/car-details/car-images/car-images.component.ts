import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-car-images',
  templateUrl: './car-images.component.html',
  styleUrls: ['./car-images.component.css']
})
export class CarImagesComponent implements OnInit {
  prefixUrl = "http://build.ford.com/dig/Ford/";
  midUrl = "/HD-FULL/";
  suffixUrl = "/EXT/1/vehicle.png?r=NaN";
  imgUrl = "/";
  constructor() { }
  @Input() carImageData;
  @Input() modelYear;
  @Input() modelName;
  ngOnInit() {
  }

}
