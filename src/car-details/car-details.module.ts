import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {NgbModalModule} from '@ng-bootstrap/ng-bootstrap';

import { CarImagesComponent } from './car-images/car-images.component';
import { CarDataComponent } from './car-data/car-data.component';
import { PriceSummaryComponent } from './price-summary/price-summary.component';
import { VehicleDetailsComponent } from './vehicle-details/vehicle-details.component';
import { CarDetailsComponent } from './car-details/car-details.component';
import { InternetPriceComponent } from './internet-price/internet-price.component';

@NgModule({
  imports: [
    CommonModule,
    NgbModalModule
  ],
  declarations: [CarImagesComponent, CarDataComponent, PriceSummaryComponent, VehicleDetailsComponent, CarDetailsComponent, InternetPriceComponent],
  exports: [CarDetailsComponent]
})
export class CarDetailsModule { }
