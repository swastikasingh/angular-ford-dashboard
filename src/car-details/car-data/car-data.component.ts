import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-car-data',
  templateUrl: './car-data.component.html',
  styleUrls: ['./car-data.component.css']
})
export class CarDataComponent implements OnInit {

  constructor() { }

  @Input() modelYear;
  @Input() modelName;
  @Input() trimName;
  @Input() price;
  @Input() imageToken;
  
  ngOnInit() {
  }

}
