import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class CarDetailsService {

  constructor(private http: HttpClient) { }
  getCarData(url) {
    return this.http.get(url);
  }
}
