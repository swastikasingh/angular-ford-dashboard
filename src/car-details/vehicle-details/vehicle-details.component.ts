import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-vehicle-details',
  templateUrl: './vehicle-details.component.html',
  styleUrls: ['./vehicle-details.component.css']
})
export class VehicleDetailsComponent implements OnInit {

  constructor() { }

  @Input() engine;
  @Input() driveTrain;
  @Input() transmission;
  @Input() exteriorColor;
  @Input() interiorColor;
  ngOnInit() {
  }

}
