import { Component, OnInit, Input } from '@angular/core';
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
import { ReactiveFormsModule } from '@angular/forms';

@Component({
  selector: 'app-internet-price',
  templateUrl: './internet-price.component.html',
  styleUrls: ['./internet-price.component.css']
})
export class InternetPriceComponent implements OnInit {

  closeResult: string;
  model: any = {};
  @Input() modelYear;
  @Input() modelName;
  @Input() imageToken;
  prefixUrl = "http://build.ford.com/dig/Ford/";
  midUrl = "/HD-FULL/";
  suffixUrl = "/EXT/1/vehicle.png?r=NaN";
  imgUrl = "/";

  
  constructor(private modalService: NgbModal) { }

  open(content) {
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return  `with: ${reason}`;
    }
  }

  ngOnInit() {
  }

}