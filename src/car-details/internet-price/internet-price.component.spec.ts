import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InternetPriceComponent } from './internet-price.component';

describe('InternetPriceComponent', () => {
  let component: InternetPriceComponent;
  let fixture: ComponentFixture<InternetPriceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InternetPriceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InternetPriceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
