import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { CarDetailsService } from '../car-details.service';
import { Config } from 'protractor';

@Component({
  selector: 'app-car-details',
  templateUrl: './car-details.component.html',
  styleUrls: ['./car-details.component.css']
})
export class CarDetailsComponent implements OnInit {
  baseCarUrl;
  carData;
  constructor(private route: ActivatedRoute,private carDetailsService: CarDetailsService) { }
  
  ngOnInit() {
    this.route.queryParams
      .subscribe(params => {
        this.baseCarUrl = "http://wwwqa.servicesus.ford.com/inventory/Search.json?" +
               "make=Ford&model=F-150;Fiesta;Mustang;Fusion;Transit&year=" +
               "2017&dealerPACode=05453;09158&postalCode=90210&vin="+params.vin;
        this.carDetailsService.getCarData(this.baseCarUrl)
          .subscribe((data: Config) => {
            this.carData = data.Response.VehicleSearch.Vehicles.Vehicle
            console.log(this.carData);
        })
      });
    
  }
  
  

}
